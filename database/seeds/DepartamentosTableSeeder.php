<?php

use Illuminate\Database\Seeder;
use App\Departamento;

class DepartamentosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('departamentos')->truncate();
        
        $departamento = new Departamento();
        $departamento->nombre = 'Amazonas';
        $departamento->save();

        $departamento = new Departamento();
        $departamento->nombre = 'Antioquia';
        $departamento->save();

        $departamento = new Departamento();
        $departamento->nombre = 'Arauca';
        $departamento->save();

        $departamento = new Departamento();
        $departamento->nombre = 'Atlántico';
        $departamento->save();

        $departamento = new Departamento();
        $departamento->nombre = 'Caquetá';
        $departamento->save();

        $departamento = new Departamento();
        $departamento->nombre = 'Cauca';
        $departamento->save();

        $departamento = new Departamento();
        $departamento->nombre = 'César';
        $departamento->save();

        $departamento = new Departamento();
        $departamento->nombre = 'Chocó';
        $departamento->save();

        $departamento = new Departamento();
        $departamento->nombre = 'Córdoba';
        $departamento->save();

        $departamento = new Departamento();
        $departamento->nombre = 'Guaviare';
        $departamento->save();

        $departamento = new Departamento();
        $departamento->nombre = 'Guainía';
        $departamento->save();

        $departamento = new Departamento();
        $departamento->nombre = 'Huila';
        $departamento->save();

        $departamento = new Departamento();
        $departamento->nombre = 'La Guajira';
        $departamento->save();

        $departamento = new Departamento();
        $departamento->nombre = 'Meta';
        $departamento->save();

        $departamento = new Departamento();
        $departamento->nombre = 'Narino';
        $departamento->save();

        $departamento = new Departamento();
        $departamento->nombre = 'Norte de Santander';
        $departamento->save();

        $departamento = new Departamento();
        $departamento->nombre = 'Putumayo';
        $departamento->save();

        $departamento = new Departamento();
        $departamento->nombre = 'Quindío';
        $departamento->save();

        $departamento = new Departamento();
        $departamento->nombre = 'Risaralda';
        $departamento->save();

        $departamento = new Departamento();
        $departamento->nombre = 'San Andrés y Providencia';
        $departamento->save();

        $departamento = new Departamento();
        $departamento->nombre = 'Santander';
        $departamento->save();

        $departamento = new Departamento();
        $departamento->nombre = 'Sucre';
        $departamento->save();

        $departamento = new Departamento();
        $departamento->nombre = 'Tolima';
        $departamento->save();

        $departamento = new Departamento();
        $departamento->nombre = 'Valle del Cauca';
        $departamento->save();

        $departamento = new Departamento();
        $departamento->nombre = 'Vaupés';
        $departamento->save();

        $departamento = new Departamento();
        $departamento->nombre = 'Vichada';
        $departamento->save();

        $departamento = new Departamento();
        $departamento->nombre = 'Casanare';
        $departamento->save();

        $departamento = new Departamento();
        $departamento->nombre = 'Cundinamarca';
        $departamento->save();

        $departamento = new Departamento();
        $departamento->nombre = 'Distrito Especial';
        $departamento->save();

        $departamento = new Departamento();
        $departamento->nombre = 'Caldas';
        $departamento->save();

        $departamento = new Departamento();
        $departamento->nombre = 'Magdalena';
        $departamento->save();

        $departamento = new Departamento();
        $departamento->nombre = 'Bolívar';
        $departamento->save();
  
        $departamento = new Departamento();
        $departamento->nombre = 'Boyacá';
        $departamento->save();
    }
}
