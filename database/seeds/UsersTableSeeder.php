<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table("users")->truncate();

        $user = User::create(array(
            'nombre' => 'Nicolas',
            'apellido' => 'Albarran',
            'email' => 'pruebas@gmail.com',
            'password' => Hash::make("123456")

        ));
    }
}
